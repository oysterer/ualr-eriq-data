# UALR ERIQ synthetic datasets #

This repository contains directories, each one containing a synthetic dataset that can be used for research and development in the area of entity resolution and master data management.

### Description of each dataset ###

* /data1 - first dataset
* /data2 - second dataset


